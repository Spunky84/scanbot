#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from signal import signal, SIGINT, SIGTERM
from logging import getLogger


class GracefulKiller(object):
    __hive = {}
    __instance = 0

    def __init__(self, loglevel='WARNING'):
        if self.__instance <= 1:
            self.__dict__ = self.__hive
            self.__instance += 1
            self.kill_now = False
            signal(SIGINT, self.termination)
            signal(SIGTERM, self.termination)
        self.log = getLogger(self.__class__.__name__)
        self.log.setLevel(loglevel)
        self.log.info('init instance {}'.format(self.__instance))
        self.log.debug('init done')

    def __del__(self):
        self.log.info('del instance {}'.format(self.__instance))
        self.__instance -= 1
        self.log.debug('del complete')

    def termination(self, signum, frame):
        self.log.warning('Termination requested with signal {}'.format(signum))
        self.log.warning('initiating shutdown')
        self.kill_now = True
