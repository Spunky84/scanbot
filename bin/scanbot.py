#!/usr/bin/env python3

from housekeeping import GracefulKiller
from os import path, listdir, remove
from shutil import move, copy
from subprocess import Popen
import time
import shlex

#globals
incomming_dir = "/scanbot/data/"
brotherID = "BRW3468951D525C"
front_prefix = "vorn"
back_prefix = "hinten"

def daemon_run():
    killer = GracefulKiller()
    print("scanbot is running")
    while True:
        if killer.kill_now:
            print("scanbot is shutting down")
            break
        files = listdir(path.normpath(incomming_dir))
        for file in files:
            if file.startswith(brotherID):
                full_path = path.join(incomming_dir, file)
                gmtime = time.gmtime(path.getctime(full_path))
                file_date = time.strftime("%Y-%m-%d", gmtime)
                file_time = time.strftime("%H_%M_%S", gmtime)
                new_file_name = file_date + " scanned " + file_time + ".pdf"
                new_full_path = path.join(incomming_dir, new_file_name)
                move(full_path, new_full_path)
            elif file.startswith(back_prefix):
                time.sleep(10)
                back_file_path = path.join(incomming_dir, file)
                back_file_time = time.gmtime(path.getctime(back_file_path))
                front_in_set = False
                for each in files:
                    if each.startswith(front_prefix):
                        front_in_set = True
                        front_file = each
                        front_file_path = path.join(incomming_dir, each)
                        front_file_time = time.gmtime(path.getctime(front_file_path))
                        break
                if front_in_set and front_file_time < back_file_time:
                    file_date = time.strftime("%Y-%m-%d", back_file_time)
                    file_time = time.strftime("%H_%M_%S", back_file_time)
                    new_file_name = file_date + " scanned " + file_time + ".pdf"
                    new_file_path = path.join(incomming_dir, new_file_name)
                    cmd = 'pdftk A="{}" B="{}" shuffle A Bend-1 output "{}"'.format(front_file_path, back_file_path, new_file_path)
                    cmd = shlex.split(cmd)
                    try:
                        merge_pdf = Popen(cmd)
                        merge_pdf.wait()
                    except:
                        copy(front_file_path, front_file_path + "_error")
                        copy(back_file_path, back_file_path + "_error")
                    remove(front_file_path)
                    remove(back_file_path)
        time.sleep(1)


if __name__ == "__main__":
    daemon_run()
